#py on peut utilise héritage pour utilise d'autre class dans obj

from Chef import Chef
from ChefJap import ChefJap

monChef = Chef()
monChef.faire_de_la_salade()

monChefJap = ChefJap()
monChefJap.faire_du_poisson()