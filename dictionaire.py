#py un dico permet de mettre une valeur avec une cle que ensuite on pourra utilise dans le code
#cree un dico 
surNom = {
    "TK" : "Triven k ",
    "Kinsu": "Kinsukee",
    "JK": "Jin kazama",
    "Nar" : 'Naruto',
    "js": "Javascript",
    "py": "python",
    "Ae": "after effect"
}


# accéder au dico
print(surNom["Kinsu"])
#2 methode pour acceder au dico
print(surNom.get('py'))